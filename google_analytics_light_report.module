<?php

/**
 * @file
 * Contains google_analytics_light_report.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function google_analytics_light_report_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the google_analytics_light_report module.
    case 'help.page.google_analytics_light_report':
      return '<pre>' . file_get_contents(dirname(__FILE__) . '/README.txt') . '</pre>';

    default:
  }
}

/**
 * Implements hook_theme().
 */
function google_analytics_light_report_theme() {

  return [
    'google_analytics_report_content' => [
      'template' => 'google-analytics-report-content',
      'variables' => [
        'data' => NULL,
      ],
    ],
    'google_analytics_report_top_browser_pie_chart' => [
      'template' => 'google-analytics-report-top-browser-pie-chart',
      'variables' => [
        'data' => NULL,
      ],
    ],
  ];

}

/**
 * Implements template_preprocess_google_analytics_report_content().
 */
function template_preprocess_google_analytics_report_content(&$variables) {
  $library_exist = google_analytics_light_report_library_exists();
  if ($library_exist) {
    $analytics = google_analytics_light_report_initialize_analytics();
    $profile_id = google_analytics_light_report_get_profile_id($analytics);
    $row_data = [];
    $page_views_data = [];
    if (!empty($profile_id)) {
      $results = $analytics->data_ga->get('ga:' . $profile_id,
          '7daysAgo',
          'today',
          'ga:pageviews',
          [
            'dimensions'  => 'ga:pagePath,ga:pageTitle',
            'filters' => 'ga:pagePath!@login;ga:pagePath!@dashboard',
          ]
        );
      $rows = $results->getRows();
      foreach ($rows as $row) {
        $row_data[] = [$row[0], $row[1], $row[2]];
      }
      $page_views_results = $analytics->data_ga->get(
        'ga:' . $profile_id,
        '7daysAgo',
        'today',
        'ga:users,ga:sessions,ga:bounceRate,ga:pageviews',
        [
          // 'dimensions'  => 'ga:day',
          // 'sort'        => '-ga:day',
          // 'max-results' => 10!
        ]
       );
      $page_views_results_rows = $page_views_results->getRows();
      foreach ($page_views_results_rows as $page_views_results_row) {
        $page_views_data = [
          'User'   => $page_views_results_row[0],
          'Seession'  => $page_views_results_row[1],
          'Bounce Rate' => number_format((float) $page_views_results_row[2], 2, '.', ''),
          'Pageviews' => $page_views_results_row[3],
        ];
      }
    }
    $build['table'] = [
      '#theme' => 'table',
      '#header' => [
        'Page Path',
        'Page Title',
        'Pageviews',
      ],
      '#rows' => $row_data,
    ];
    $output = [
      '#type' => 'markup',
      '#markup' => drupal_render($build),
    ];
    $data = ['page_views' => $output, 'page_views_data' => $page_views_data];
    $variables['data'] = $data;
  }
  $variables['#attached']['library'][] = 'google_analytics_light_report/google_analytics_report_content';
}

/**
 * Function to get day option list.
 */
function google_analytics_light_report_days_options_list() {
  $list = [];
  $list['7daysAgo'] = t('Last 7 days');
  $list['14daysAgo'] = t('Last 14 days');
  $list['28daysAgo'] = t('Last 28 days');
  $list['30daysAgo'] = t('Last 30 days');
  $list['90daysAgo'] = t('Last 90 days');
  $list['180daysAgo'] = t('Last 180 days');
  $list['365daysAgo'] = t('Last 365 days');
  return $list;
}

/**
 * Function to check google-api-php-client library exists.
 */
function google_analytics_light_report_library_exists() {
  $path = FALSE;
  // Add libraries supported path.
  $path = function_exists('libraries_get_path') ? libraries_get_path('google-api-php-client') : 'libraries/google-api-php-client';
  if (!empty($path) && file_exists($path . '/vendor/autoload.php')) {
    include_once $path . '/vendor/autoload.php';
    module_load_include('inc', 'google_analytics_light_report', 'includes/google_analytics_light_report');
  }
  else {
    drupal_set_message(t("google-api-php-client library for Google Analytics Report Light is not properly installed. For details check the 'Requirements' section of README.txt."), 'warning');
    $path = FALSE;
  }
  return $path;
}
